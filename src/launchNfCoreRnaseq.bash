#!/usr/bin/env bash

# https://nf-co.re/rnaseq/parameters
# https://umi-tools.readthedocs.io/en/latest/the_methods.html

species=$1
if [ -z "$species" ]
then
	echo "ERROR: you need to specify the species"
	exit 1
fi
echo ${species}
faF=""
gffF=""
if [[ $species = "pea" ]]
then
	faF="Pisum_sativum_v1a.fa"
	gffF="Pisum_sativum_v1a_genes.gff"
elif [[ $species = "wheat" ]]
then
	faF="iwgsc_refseqv2.1_assembly.fa"
	gffF="iwgsc_refseqv2.1_annotation_200916_HC.gff"
elif [[ $species = "maize" ]]
then
	faF="Zm-B73-REFERENCE-NAM-5.0.fa"
	gffF="Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.gff"
elif [[ $species = "bean" ]]
then
	faF="Pvulgaris_442_v2.0.softmasked.fa"
	gffF="Pvulgaris_442_v2.1.gene_exons.gff"
fi
if [[ ! -f ${faF} ]]
then
	echo "ERROR: can't find file ${faF}"
	exit 1
fi
if [[ ! -f ${gffF} ]]
then
	echo "ERROR: can't find file ${gffF}"
	exit 1
fi

nextflow run nf-core/rnaseq \
	       -c configNfCoreRnaseq.txt \
	       -r "3.10.1" \
	       -resume \
	       --input samplesheet_${species}.csv \
	       --outdir out_nfCore_${species} \
         --multiqc_title "intercropRNAseq ${species}" \
	       --with_umi \
	       --skip_umi_extract \
	       --umitools_grouping_method "unique" \
         --umitools_dedup_stats \
         --fasta ${faF} \
	       --gff ${gffF} \
	       --save_reference \
         --skip_trimming \
         --aligner star_salmon \
	       --bam_csi_index \
         --seq_center "IPS2 POPS" \
	       --skip_biotype_qc
