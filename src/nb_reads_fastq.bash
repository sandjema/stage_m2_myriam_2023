#!/usr/bin/env bash

species=$1
if [ -z "$species" ]
then
	echo "ERROR: you need to specify the species"
	exit 1
fi

outF="nb_reads_fastq_${species}.txt"
echo -e "species\tfile\tnbReads" > ${outF}

fastqDir="/data/gqe/deap/intercroprnaseq/rnaseq_2022/after_trim/"
ls "${fastqDir}/${species}" | while read fastqgz; do
  nbReads=$(zcat ${fastqDir}/${species}/${fastqgz} | grep -c "^@")
  echo -e "${species}\t${fastqgz}\t${nbReads}" >> ${outF}
done
