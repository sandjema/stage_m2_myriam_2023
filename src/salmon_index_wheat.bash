#!/bin/bash -ue

grep '^>' iwgsc_refseqv2.1_assembly.fa | cut -d ' ' -f 1 > decoys.txt

sed -i.bak -e 's/>//g' decoys.txt

cat genome.transcripts.fa iwgsc_refseqv2.1_assembly.fa > gentrome.fa

salmon \
    index \
    --threads 12 \
    -t gentrome.fa \
    -d decoys.txt \
    -i iwgsc_refseq2.1_salmon_index
