#!/usr/bin/env bash

# run this script inside results/

datDir="/data/gqe/deap/intercroprnaseq/rnaseq_2022/after_trim"

echo -e "sample,fastq_1,fastq_2,strandedness" > samplesheet_wheat.csv
cat plate1_corresp-tubes-samples.csv | awk -v datDir=$datDir -v OFS="," '{if($7=="wheat"){print $1,datDir"/wheat/"$14_"_bbdukTrimmed.fastq.gz","","auto"}}' >>  samplesheet_wheat.csv

echo -e "sample,fastq_1,fastq_2,strandedness" > samplesheet_pea.csv
cat plate1_corresp-tubes-samples.csv | awk -v datDir=$datDir -v OFS="," '{if($7=="pea"){print $1,datDir"/pea/"$14_"_bbdukTrimmed.fastq.gz","","auto"}}' >> samplesheet_pea.csv 

echo -e "sample,fastq_1,fastq_2,strandedness" > samplesheet_maize.csv
cat plate1_corresp-tubes-samples.csv | awk -v datDir=$datDir -v OFS="," '{if($7=="maize"){print $1,datDir"/maize/"$14_"_bbdukTrimmed.fastq.gz","","auto"}}' >> samplesheet_maize.csv

echo -e "sample,fastq_1,fastq_2,strandedness" > samplesheet_bean.csv
cat plate1_corresp-tubes-samples.csv | awk -v datDir=$datDir -v OFS="," '{if($7=="bean"){print $1,datDir"/bean/"$14_"_bbdukTrimmed.fastq.gz","","auto"}}' >> samplesheet_bean.csv
