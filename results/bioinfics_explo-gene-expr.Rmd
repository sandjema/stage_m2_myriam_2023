---
title: "Exploration of gene expression data"
author: "T. Flutre"
date: "`r format(Sys.time(), '%d/%m/%Y %H:%M:%S')`"
lang: "en"
colorlinks: true
output:
  html_document:
    toc: true
    toc_depth: 5
    toc_float: true
    number_sections: TRUE
    code_folding: show
  pdf_document:
    toc: true
    toc_depth: 3
    number_sections: TRUE
urlcolor: blue
editor_options: 
  chunk_output_type: console
---

<!--
This R chunk is used to set up some options.
-->
```{r setup, include=FALSE}
suppressPackageStartupMessages(library(knitr))
opts_chunk$set(echo=TRUE, warning=TRUE, message=TRUE, cache=FALSE,
               fig.align="center")
opts_knit$set(progress=TRUE, verbose=TRUE)
```


# Overview

This document is part of the "bioinfics" project.

Paths:
```{r}
projectName <- "bioinfics"
projectDir <- ""
if(Sys.info()["user"] == "tflutre"){
  if(Sys.info()["nodename"] == "portdeap2"){
    projectDir <- file.path("~/Documents/travail/CR/projets_GQE-DEAP/intercropRNAseq",
                            projectName)
  } else if(Sys.info()["nodename"] == "smith-neo")
	  projectDir <- file.path("~/work",
				  projectName)
} else if(Sys.info()["user"] == "stagiaire"){
  projectDir <- file.path("~/Documents/stageMSaid",
                           projectName)
} else if(Sys.info()["user"] == "msaid" &
          Sys.info()["nodename"] == "smith-neo"){
  projectDir <- file.path("~/work",
                           projectName)
}
projectDir <- path.expand(projectDir)
stopifnot(dir.exists(projectDir))
dataDir <- file.path(projectDir, "data")
stopifnot(dir.exists(dataDir))
resultsDir <- file.path(projectDir, "results")
stopifnot(dir.exists(resultsDir))
srcDir <- file.path(projectDir, "src")
## stopifnot(dir.exists(srcDir))
```

Dependencies:
```{r}
suppressPackageStartupMessages(library(ggplot2))
suppressPackageStartupMessages(library(FactoMineR))
suppressPackageStartupMessages(library(umap))
```

Execution time (see the appendix):
```{r time_0}
t0 <- proc.time()
```


# Load all the data

## Sample annotations

First `bioinfics_make-sample-table.Rmd` must be executed beforehand:
```{r}
inF <- file.path(resultsDir, "plate1_corresp-tubes-samples.csv")
splInfos <- read.table(inF, header=TRUE, sep="\t", stringsAsFactors=TRUE)
splInfos$date <- factor(as.character(splInfos$date))
splInfos$stand <- factor(as.character(splInfos$stand), levels=c("pure","mixed"))
str(splInfos)
tapply(splInfos$sample, splInfos$species, length)
```

## Gene-level read counts

Based on the doc of nf-core/rnaseq, with 3’ tagged RNA-seq data, it is [advised](https://nf-co.re/rnaseq/3.10.1/output#salmon) to use the `salmon.merged.gene_counts.tsv` files with DESeq2, even though these data are not corrected for potential differential isoform usage.

It should be based on the docs to make the analysis with our data in 3' tagged RNA-seq, [https://bioconductor.org/packages/devel/bioc/vignettes/tximport/inst/doc/tximport.html#3%E2%80%99_tagged_RNA-seq] 
data is normalized with transcription per million (TMP)

```{r}
gReadCounts <- list()
for(species in c("wheat", "pea", "maize", "bean")){
  inF <- file.path(dataDir, "read_counts",
                   paste0(species, "_salmon.merged.gene_counts.tsv"))
  if(! file.exists(inF))
    next
  gReadCounts[[species]] <- read.delim(inF, header=TRUE, sep="\t")
}
str(gReadCounts)
sapply(gReadCounts, dim)
```


# Reformat the data

```{r}
species <- "pea"

splInfo <- droplevels(splInfos[splInfos$species == species,])
rownames(splInfo) <- splInfo$sample
splInfo$date <- factor(sub("22$", "", as.character(splInfo$date)),
                        levels=c("3003", "2405"))
levels(splInfo$date) <- c("30/03", "24/05")
splInfo$id <- factor(paste0(splInfo$date, "_", splInfo$standName, "_", splInfo$block))
splInfo <- splInfo[, c("id","sample","date","focal","neighbor","standName","stand","block","x","y")]
str(splInfo)
head(splInfo)

dat <- t(gReadCounts[[species]][, -c(1:2)])
colnames(dat) <- gReadCounts[["pea"]][, "gene_id"]

stopifnot(all(sort(rownames(dat)) == sort(rownames(splInfo))))
dat <- cbind(splInfo[rownames(dat),], dat)
rownames(dat) <- dat$id
dat$id <- NULL

dim(dat)
dat[1:3, 1:10]
```


# Explore the data

## PCA

```{r}
## suppressPackageStartupMessages(library(rutilstimflutre))
## res_pcaT <- pca(dat[, -c(1:8)], ct=TRUE, sc=FALSE, plot=NULL)
```

http://factominer.free.fr/factomethods/analyse-en-composantes-principales.html
```{r, fig.width=12, fig.height=12}
if(FALSE){
  res_pca <- PCA(dat, scale.unit=TRUE, ncp=5, graph=FALSE, quali.sup=1:8)
  plot.PCA(res_pca, axes=c(1,2), choix="ind", habillage=1)
  ## Error in `check_aesthetics()`:
  ## ! Aesthetics must be either length 1 or the same as the data (2): hjust and vjust
  ## Run `]8;;rstudio:run:rlang::last_error()rlang::last_error()]8;;` to see where the error occurred.
}

## color per date
idxToKeep <- which(colnames(dat) == "date")
idxToRmv <- (1:10)[(1:10) != idxToKeep]
res_pca <- PCA(dat[, -idxToRmv], scale.unit=TRUE, ncp=5, graph=FALSE, quali.sup=1)
plot.PCA(res_pca, axes=c(1,2), choix="ind", habillage=1)

## color per stand
idxToKeep <- which(colnames(dat) == "stand")
idxToRmv <- (1:10)[(1:10) != idxToKeep]
res_pca <- PCA(dat[, -idxToRmv], scale.unit=TRUE, ncp=5, graph=FALSE, quali.sup=1)
plot.PCA(res_pca, axes=c(1,2), choix="ind", habillage=1)

## color per geno
idxToKeep <- which(colnames(dat) == "focal")
idxToRmv <- (1:10)[(1:10) != idxToKeep]
res_pca <- PCA(dat[, -idxToRmv], scale.unit=TRUE, ncp=5, graph=FALSE, quali.sup=1)
plot.PCA(res_pca, axes=c(1,2), choix="ind", habillage=1)
```

## UMAP

```{r, fig.width=10}
umap.defaults
umap_custom <- umap.defaults
umap_custom$random_state <- 54321
res_u <- umap(dat[, -c(1:10)], umap_custom)

colnames(res_u$layout) <- paste0("dim", 1:2)
tmp <- cbind(dat[,c(1:10)], res_u$layout)
tmp$dateFoc <- paste0(tmp$date, "_", tmp$focal)
tmp$dateFocNei <- paste0(tmp$date, "_", tmp$focal, "-", tmp$neighbor)
tmp$focSt <- paste0(tmp$focal, "_", tmp$stand)

ggplot(tmp, aes(x=dim1, y=dim2, color=focSt, shape=date)) +
  labs(title=paste0(projectName, ": ", "wheat-pea"),
       x="dim 1 of UMAP", y="dim 2 of UMAP") +
  geom_hline(yintercept=0) +
  geom_point(size=3)
```

## HC

https://www.datacamp.com/tutorial/hierarchical-clustering-R
```{r, fig.width=10}
dist_mat <- dist(scale(dat[, -c(1:10)]), method="euclidean")
res_hc_avg <- hclust(dist_mat, method="average")
plot(res_hc_avg)
```



# Appendix

```{r info}
t1 <- proc.time(); t1 - t0
print(sessionInfo(), locale=FALSE)
```
